import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import { appendFile } from "fs";
import { request } from "http";

dotenv.config();

const server: Express = express();

const port: string = process.env.PORT || '9001';

server.get('/', (req: Request, res: Response) =>{
    res.send('Hello World!');
})

server.listen(port, () => {
    console.log(`Server listening on port http://0.0.0.0:${port}`);
})